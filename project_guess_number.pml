<?xml version="1.0" encoding="UTF-8" ?>
<Package name="project_guess_number" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="guess_number" src="guess_number/guess_number.dlg" />
        <Dialog name="get_number_human" src="get_number_human/get_number_human.dlg" />
        <Dialog name="get_number_pepper" src="get_number_pepper/get_number_pepper.dlg" />
    </Dialogs>
    <Resources>
        <File name="index" src="html/index.html" />
        <File name="script" src="html/script.js" />
        <File name="typed.min" src="html/typed.min.js" />
    </Resources>
    <Topics>
        <Topic name="guess_number_czc" src="guess_number/guess_number_czc.top" topicName="guess_number" language="cs_CZ" />
        <Topic name="get_number_human_czc" src="get_number_human/get_number_human_czc.top" topicName="get_number_human" language="cs_CZ" />
        <Topic name="get_number_pepper_czc" src="get_number_pepper/get_number_pepper_czc.top" topicName="get_number_pepper" language="cs_CZ" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="cs_CZ">
        <Translation name="translation_cs_CZ" src="translations/translation_cs_CZ.ts" language="cs_CZ" />
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
