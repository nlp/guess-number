# Guess number/ Myslím si číslo

Let´s play a game "Guess number" with robot Pepper. Only Czech language is currently supported.

## guess_number dialog

Start the game by saing "myslím si číslo" or "mysli si číslo". 

Pepper is going to ask you some questions: 
1.  if you are going to play the game "myslím si číslo"
2.  the highest number of the interval in which the guessing number is going to be from
3.  who is going to be guessing

If the robot is guessing your number, you will be asked to think a number from the interval. Then you can confirm by saing one of these words: "mám", "jo", "ano".
If you are guessing robot´s number he randomly chooses a number from the interval.

## get_number_pepper python script

Script is called when Pepper is guessing your number. Pepper takes the initial interval and ask if the middle number from the interval is lower, higher or equal. 
Then adjust the interval by your answer. Repeats until your answer is: equal.

## get_number_human python script

Script is called when you are guessing Pepper´s number. Pepper asks you to say a number which you guess is the number he is thinkig. 
He will answer if your number is correct and you´ve found his number or he will tell you if his number is higher or lower than your guess. Repeats until your guess is correct.

## animation Happy

At the end of dialog Pepper makes a happy dance.

## tablet displaying the currant interval 

Robot uses the tablet for displaying the currant interval in which the guessing number is.
Project contains code copied from [Salisu Wada github](https://github.com/salisuwy/Pepper-Robot-Displays-User-Input)
